# Visualising Data Shadows
--- meta
title: Visualising Data Shadows
uuid: 1ee3bef4-3f10-4679-a3d6-788eaafa5850
lan: en
source: Tactical Tech, adapted from an exercise written up on the [Gender and Tech Resources Community Wiki](https://gendersec.tacticaltech.org/wiki/index.php/Privacy_Analogue_data_shadows)
item: Activity
tags:
  - Mobile
  - Secure messaging
  - Browser
  - Choosing tools
  - Drawing exercise
duration: 45
description: Make data and digital shadows more tangible and personal through a drawing exercise, followed by a discussion on how digital shadows are created in our everyday lives.
---

## Meta information

### Description
Make data and digital shadows more tangible and personal through a drawing exercise, followed by a discussion on how digital shadows are created in our everyday lives.


### Duration
40 - 60 minutes.


### Ideal Number of Participants
Minimum of 4.


### Learning Objectives
##### Knowledge
- Increased understanding of digital shadows: what they are, and how they are created.
- Understand the difference between content and metadata.

##### Skills
- Be able to tie data traces to your own reality, digital identity and online activities.

### References
- [Content vs Metadata](https://myshadow.org/digital-traces-content-and-metadata), MyShadow / Tactical Tech)
- [How much control do we have over our data?](https://myshadow.org/how-much-control-do-we-have-over-our-data) (MyShadow / Tactical Tech)

### Materials and Equipment Needed
- @[material](67c7149b-44f9-4b7a-b4ee-c8bf786b50dc)
- @[material](417fc8a5-400f-4553-a23e-faa949beb239)
- @[material](5cd65e76-c2fd-4586-b889-abb5dd6cd836)


## Activity: "Data Shadows"
--- meta
uuid: 695454a4-7717-41eb-b3f9-fb0b346151ae
---

#### Preparation
1. This activity works best if participants have already done the Activity "How the Internet Works".
2. On a table, set out a variety of workshop materials: pens, paint, strings, glitter etc.

#### Optional: Polaroid exercise (15 min)
1. Take a picture of all participants with Polaroid camera
2. Write meta data on the pictures: location, time, type of camera
3. Use these Polaroids as a starting point for participants to draw their own digital shadow


#### Draw your digital shadow (10 min)
Give everyone a piece of paper and give the following instructions:
1. Draw a being with many hands. Each hand represents one of your devices (mobile, tablet, computer, etc).
2. Once the drawings have been done: For each hand/device, draw or list the data that is stored on it, and/or that is shared when you use it.

#### Compare drawings(10 min)
Break into small groups of two to four and look at each others' digital shadow drawing, giving small explanations where needed and noting down any similarities, differences, or things that stand out.

#### Discussion(10 min)
Put the following questions on the board for group discussion in the same groups.
1. How are our digital shadows created?
2. Do they represent us? How are they different to our offline identity?

#### Feedback (10 min)
1. Bring everyone together again, and ask what data traces were found. Put some of these up on the board, in one of two columns (content or metadata)
2. Give a brief explanation on content vs metadata, and why metadata is important.

#### Optional: follow-up discussion
How much control do you have over your own data?


-------------------------------
<!---
data-privacy-training/Activities/TEMPLATE
-->
